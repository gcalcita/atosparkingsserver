package atos.omar.test.junit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import atos.omar.test.Counter;
import atos.omar.test.Parking;
import atos.omar.test.ParkingDao;
import atos.omar.test.ParkingExamples;
import atos.omar.test.SQLConstants;

public class ParkingsTest {

	private final static Logger LOG = Logger.getLogger(ParkingsTest.class);
	@Before
	public void setUp() throws Exception {
		BasicConfigurator.configure();
	}
	public void createDefaultDatabase() throws Exception{
		Counter.reset();
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			List<Parking> parkings = ParkingExamples.getParkings();
			ParkingDao dao = new ParkingDao(connection, true);
			for ( Parking p: parkings ) {
				dao.addParking(p);				
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw e;
		} 
	}

	@Test
	public void testAddParking() {
		Counter.reset();
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			List<Parking> parkings = ParkingExamples.getParkings();
			ParkingDao dao = new ParkingDao(connection, true);
			int id = dao.addParking(parkings.get(0));
			LOG.debug("id: " + id);
			assert(id> 0);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			assert(false);
		}
	}
	
	@Test
	public void testSelectParking() {
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			createDefaultDatabase();
			ParkingDao dao = new ParkingDao(connection, false);
			Parking p =dao.getParking(2);
			assert("P2".equals(p.getName())  && p.getOpeningDays().contains(5));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			assert(false);
		}
	}
	
	
	@Test
	public void testUpdateParking() {
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			createDefaultDatabase();
			ParkingDao dao = new ParkingDao(connection, false);
			Parking p =dao.getParking(2);
			p.setName("P2Test");
			p.addOpeningDay(7);
			dao.updateParking(p);
			Parking pPost =dao.getParking(2);
			assert("P2Test".equals(pPost.getName())  && pPost.getOpeningDays().contains(7));
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			assert(false);
		}
	}
	
	@Test
	public void testFreePlace1() {
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			createDefaultDatabase();
			ParkingDao dao = new ParkingDao(connection, false);
			int error = dao.freePlace(3);
			assert(error == ParkingDao.ERROR_PARKING_EMPTY);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			assert(false);
		}
	}
	@Test
	public void testFreePlace2() {
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			createDefaultDatabase();
			ParkingDao dao = new ParkingDao(connection, false);
			int error = dao.takePlace(2);
			assert(error == ParkingDao.NO_ERROR);
			error = dao.freePlace(2);
			assert(error == ParkingDao.NO_ERROR);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			assert(false);
		}
	}
	

	@Test
	public void testTakePlace1() {
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			createDefaultDatabase();
			ParkingDao dao = new ParkingDao(connection, false);
			int error = dao.takePlace(2);
			assert(error == ParkingDao.NO_ERROR);
			Parking p =dao.getParking(2);
			assert(p.getFreePlaces() == 2);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			assert(false);
		}
	}
	
	@Test
	public void testTakePlace2() {
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			createDefaultDatabase();
			ParkingDao dao = new ParkingDao(connection, false);
			int error = dao.takePlace(3);
			assert(error == ParkingDao.NO_ERROR);
			error = dao.takePlace(3);
			assert(error == ParkingDao.ERROR_PARKING_FULL);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			assert(false);
		}
	}
	
	@Test
	public void testGetOpenParkings() {
		//TODO funciona según la hora en que se ejecute
		try ( Connection connection = DriverManager.getConnection(SQLConstants.CONNECTION_STRING)){
			createDefaultDatabase();
			ParkingDao dao = new ParkingDao(connection, false);
			List<Parking> open = dao.getOpenParkings(false);
			assert(open.size() == 2);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			assert(false);
		}
	}
	

}
