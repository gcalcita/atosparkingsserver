package atos.omar.test;

public class Counter {
	private static Integer currentMax = 0;
	private static Counter counter;
	private Counter ( ) {
	}

	public static Counter getInstance( ) {
		if ( counter == null ) {
			counter = new Counter();
		}
		return counter;
	}
	
	public int nextId() {
		return ++currentMax;
	}
	
	public static void reset() {
		currentMax = 0;		
	}
	
	
}
