package atos.omar.test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "parking")
public class Parking implements Serializable {
	
	private static final long serialVersionUID = -7633204168986689310L;
	private int id;
	private String name;
	//0-23
	private int openHour;
	//1-24
	private int closeHour;
	private int size;
	private int freePlaces;
	private Collection<Integer> openingDays;
	private double latitude;
	private double longitude;
	
	public Parking(String name, int openHour, int closeHour, int size, Integer[] openingDays,
			double latitude, double longitude) {
		this(name, openHour, closeHour, size, Arrays.asList(openingDays), latitude, longitude);
	}
	public Parking(String name, int openHour, int closeHour, int size, List<Integer> openingDays,
			double latitude, double longitude) {
		this(name, openHour, closeHour, size, latitude, longitude);
		this.openingDays = openingDays;
	}
	
	public Parking(String name, int openHour, int closeHour, int size, double latitude, double longitude) {
		this(Counter.getInstance().nextId(), name, openHour, closeHour, size, size, latitude, longitude);
	}
	
	public Parking(int id, String name, int openHour, int closeHour, int size, int freePlaces, double latitude, double longitude) {
		this.id = id;
		this.name = name;
		this.openHour = openHour;
		this.closeHour = closeHour;
		this.size = size;
		this.freePlaces = freePlaces;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	@XmlElement
	public void setName(String name) {
		this.name = name;
	}
	public int getOpenHour() {
		return openHour;
	}
	@XmlElement
	public void setOpenHour(int openHour) {
		this.openHour = openHour;
	}
	public int getCloseHour() {
		return closeHour;
	}
	@XmlElement
	public void setCloseHour(int closeHour) {
		this.closeHour = closeHour;
	}
	public int getSize() {
		return size;
	}
	@XmlElement
	public void setSize(int size) {
		this.size = size;
	}
		
	public int getFreePlaces() {
		return freePlaces;
	}
	public void setFreePlaces(int freePlaces) {
		this.freePlaces = freePlaces;
	}
	public Collection <Integer> getOpeningDays() {
		return openingDays;
	}
	@XmlElement
	public void setOpeningDays(List<Integer> openingDays) {
		this.openingDays = openingDays;
	}
	public double getLatitude() {
		return latitude;
	}
	@XmlElement
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	@XmlElement
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public void addOpeningDay( int weekDay ) {
		if ( openingDays == null ) {
			openingDays = new ArrayList<Integer>();
		}
		openingDays.add(weekDay);
	}
	
	@Override
	public String toString() {
		return "Parking [id=" + id + ", name=" + name + ", openHour=" + openHour + ", closeHour=" + closeHour
				+ ", size=" + size + ", freePlaces=" + freePlaces + ", openingDays=" + openingDays + ", latitude="
				+ latitude + ", longitude=" + longitude + "]";
	}
	
	
}
