package atos.omar.test;

import java.util.List;

import javax.jws.WebService;

@WebService
public interface ParkingService {
	public int addParking ( Parking parking );
	public void updateParking ( Parking parking );
	public List<Parking> getOpenParkings(boolean completed, String day);
	public void takePlace ( int idParking );
	public void freePlace ( int idParking );
	

}
