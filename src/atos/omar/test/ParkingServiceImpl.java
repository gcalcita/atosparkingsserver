package atos.omar.test;
import java.util.List;

import javax.jws.WebService;

@WebService(endpointInterface = "atos.omar.test.ParkingService")
public class ParkingServiceImpl implements ParkingService {
	
	//TODO TRANSACCIONES, VALIDACIONES, 

	private ParkingDao parkingDao;// = new ParkingDao();
	
	//devolver id
	public int addParking ( Parking parking ) {
		return parkingDao.addParking(parking);
	}
	public void updateParking ( Parking parking ) {
		parkingDao.updateParking(parking);
	}
	//[OPCIONAL] Dado una latitud y longitud devolver los eventos
	public List<Parking> getOpenParkings(boolean completed, String day) {
		return parkingDao.getOpenParkings(completed, day);
	}
	
	public void takePlace ( int idParking ) {
		parkingDao.takePlace(idParking);
	}
	public void freePlace ( int idParking ) {
		parkingDao.freePlace(idParking);
	}
	
	
}
