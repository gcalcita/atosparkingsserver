package atos.omar.test;

public class SQLConstants {
	//TODO índices, integridad referencial, esquema, prefijo de tablas
	public static final String CREATE_TABLE_PARKING = "CREATE TABLE PARKING (ID INT PRIMARY KEY, NAME VARCHAR(25), OPENHOUR INT, CLOSEHOUR INT, SIZE_ INT, FREE_PLACES INT, LATITUDE DOUBLE, LONGITUDE DOUBLE)";
	//días de la semana Domingo =1 Sabado = 7
	public static final String CREATE_TABLE_PARKING_DAYS = "CREATE TABLE PARKING_DAYS ( IDPARKING INT, OPENDAY INT )";
	public static final String CONNECTION_STRING = "jdbc:h2:mem:testParking";
	public static final String DROP_TABLE_PARKING = "DROP TABLE PARKING IF EXISTS";
	public static final String DROP_TABLE_PARKING_DAYS = "DROP TABLE PARKING_DAYS IF EXISTS";
	public static final String INSERT_PARKING = "INSERT INTO PARKING (ID, NAME, OPENHOUR, CLOSEHOUR, SIZE_, FREE_PLACES, LATITUDE, LONGITUDE) VALUES(?, ?, ?, ?, ?, ?,?, ?)";
	public static final String INSERT_PARKING_DAYS = "INSERT INTO PARKING_DAYS (IDPARKING, OPENDAY) values (?,?)";
	public static final String DELETE_PARKING_DAYS = "DELETE FROM PARKING_DAYS WHERE IDPARKING = ?";
	public static final String UPDATE_TAKE_PLACE = "UPDATE PARKING SET FREE_PLACES = FREE_PLACES -1 WHERE ID = ?";
	public static final String UPDATE_FREE_PLACE = "UPDATE PARKING SET FREE_PLACES = FREE_PLACES +1 WHERE ID = ?";
	public static final String SELECT_PARKING = "SELECT ID, NAME, OPENHOUR, CLOSEHOUR, SIZE_, FREE_PLACES, LATITUDE, LONGITUDE FROM PARKING WHERE ID = ?";
	public static final String UPDATE_PARKING = "UPDATE PARKING SET  NAME = ?, OPENHOUR = ?, CLOSEHOUR = ?, SIZE_ = ?, FREE_PLACES = ? ,LATITUDE = ?, LONGITUDE = ? WHERE ID = ?";
	public static final String SELECT_PARKINGS_FROM_DAY = "SELECT ID, NAME, OPENHOUR, CLOSEHOUR, SIZE_, FREE_PLACES, LATITUDE, LONGITUDE"
			+ " FROM PARKING INNER JOIN PARKING_DAYS ON PARKING.ID= PARKING_DAYS.IDPARKING"
			+ " WHERE PARKING_DAYS.OPENDAY = ? AND OPENHOUR <=? AND CLOSEHOUR > ?";
	public static final String SELECT_PARKING_DAYS = " SELECT OPENDAY FROM PARKING_DAYS WHERE IDPARKING = ?";
}
