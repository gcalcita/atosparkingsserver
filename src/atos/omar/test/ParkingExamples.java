package atos.omar.test;

import java.util.ArrayList;
import java.util.List;

public class ParkingExamples {
	
	public static List<Parking> getParkings() {
		List<Parking> parkings = new ArrayList<Parking>();
		
		Integer[] days1 ={1,2,3,4};
		Parking p1 = new Parking("P1", 10, 23, 100, days1, 0, 0);
		parkings.add(p1);
		Integer[] days2 ={1, 2,3,4,5};
		Parking p2 = new Parking("P2", 10, 14, 3, days2, 0, 0);
		parkings.add(p2);
		
		Integer[] days3 ={2,3,4,5,6,7};
		Parking p3 = new Parking("P3", 13, 24, 1, days3, 0, 0);
		parkings.add(p3);
		return parkings;
	}

}
