package atos.omar.test;

import java.util.Calendar;

import javax.xml.bind.DatatypeConverter;

public class Utils {
	
	/**
	 *  @param day en formato ISO-8601 (ejemplo: ‘2015-03-01T10:00:00+02:00’)
	 * @return WeekDay
	 */
	public static int getWeekDayFromDate( String day ) {
		Calendar cal = DatatypeConverter.parseDateTime(day ) ;
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	public static int getHourFloor( String day ) {
		Calendar cal = DatatypeConverter.parseDateTime(day ) ;
		return cal.get(Calendar.HOUR_OF_DAY);
	}
	public static int getHourCeiling( String day ) {
		Calendar cal = DatatypeConverter.parseDateTime(day ) ;
		int hour =  cal.get(Calendar.HOUR_OF_DAY);
		if (  cal.get(Calendar.MINUTE) > 0 ||  cal.get(Calendar.SECOND) > 0 ) {
			hour++;
		}
		return hour;
	}

}
