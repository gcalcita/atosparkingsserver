package atos.omar.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;


public class ParkingDao {
	private final static Logger LOG = Logger.getLogger(ParkingDao.class);
	
	public static final int NO_ERROR = 0;
	public static final int ERROR_PARKING_FULL = 1;
	public static final int ERROR_PARKING_EMPTY = 2;
	public static final int ERROR_PARKING_NOT_EXISTS = 3;
	public static final int ERROR_UNDEFINED = 4;
	
	private Connection connection;
	
	
	public ParkingDao( Connection connection, boolean createDatabase)  {
		this.connection = connection;
		if ( createDatabase ) {
			dropTables();
			createTables();
		}
	}
	
	private void dropTables () {
		try (   PreparedStatement st = getConnection().prepareStatement(SQLConstants.DROP_TABLE_PARKING); 
				PreparedStatement st2 = connection.prepareStatement(SQLConstants.DROP_TABLE_PARKING_DAYS)
			) {	
			st.execute();
			st2.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
	}
	private void createTables () {
		try (   PreparedStatement st = getConnection().prepareStatement(SQLConstants.CREATE_TABLE_PARKING); 
				PreparedStatement st2 = connection.prepareStatement(SQLConstants.CREATE_TABLE_PARKING_DAYS)
			) {	
			st.execute();
			st2.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
	}
	
	/**
	 * 
	 * @param parking
	 * @return parking id
	 * @throws Exception
	 * TODO validaciones
	 */
	public int addParking ( Parking parking ) {
		try (   PreparedStatement st = getConnection().prepareStatement(SQLConstants.INSERT_PARKING); 
				PreparedStatement st2 = connection.prepareStatement(SQLConstants.INSERT_PARKING_DAYS)
			) {
			st.setInt(1, parking.getId());
			st.setString(2,  parking.getName());
			st.setInt(3, parking.getOpenHour());
			st.setInt(4, parking.getCloseHour());
			st.setInt(5, parking.getSize());
			st.setInt(6, parking.getFreePlaces());
			st.setDouble(7, parking.getLatitude());
			st.setDouble(8, parking.getLongitude());
			st.execute();
			LOG.debug("addParking id: " + parking.getId());
			for ( int weekDay : parking.getOpeningDays() ) {
				st2.setInt(1, parking.getId());
				st2.setInt(2, weekDay);			
				st2.execute();
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return parking.getId();
	}
	public void updateParking ( Parking parking ) {
		/*
		 * TODO validaciones
		 * horas y coordenadas valores válidos
		 * size >0 freePlaces>=0 freeplaces<=size
		 */
		
		try (   PreparedStatement st1 = getConnection().prepareStatement(SQLConstants.DELETE_PARKING_DAYS);
				PreparedStatement st2 = getConnection().prepareStatement(SQLConstants.UPDATE_PARKING); 
				PreparedStatement st3 = getConnection().prepareStatement(SQLConstants.INSERT_PARKING_DAYS);
			) {
			//borrar los días abiertos
			st1.setInt(1, parking.getId());
			st1.execute();
			//actualizar parking
			st2.setString(1,  parking.getName());
			st2.setInt(2, parking.getOpenHour());
			st2.setInt(3, parking.getCloseHour());
			st2.setInt(4, parking.getSize());
			st2.setInt(5, parking.getFreePlaces());
			st2.setDouble(6, parking.getLatitude());
			st2.setDouble(7, parking.getLongitude());
			st2.setInt(8, parking.getId());
			st2.execute();
			
			//añadir días abierto
			for ( int weekDay : parking.getOpeningDays() ) {
				st3.setInt(1, parking.getId());
				st3.setInt(2, weekDay);
				st3.execute();
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	private PreparedStatement createPSSelectParking( int id ) throws SQLException {
		PreparedStatement st = getConnection().prepareStatement(SQLConstants.SELECT_PARKING);
		st.setInt( 1, id );
		st.execute();
		return st;
	}
	
	public Parking getParking( int id ) {
		Parking p = null;
		try ( PreparedStatement st = createPSSelectParking(id);
			  ResultSet rs = st.executeQuery(); ) {
            if(rs.next()) {
            	p = getParking(rs);
            	addOpeningDays(p);
            }
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return p;
	}

	private Parking getParking ( ResultSet rs ) throws SQLException {
       	return new Parking(rs.getInt("ID"), rs.getString("NAME"), rs.getInt("OPENHOUR"), 
    			rs.getInt("CLOSEHOUR"), rs.getInt("SIZE_"), rs.getInt("FREE_PLACES"), rs.getDouble("LATITUDE"), rs.getDouble("LONGITUDE"));
	}
	
	private PreparedStatement createPSSelectParkingDays( int id ) throws SQLException {
		PreparedStatement st = getConnection().prepareStatement(SQLConstants.SELECT_PARKING_DAYS);
		st.setInt( 1, id );
		st.execute();
		return st;
	}
	private void addOpeningDays(Parking parking) {
 		try (  
 				PreparedStatement st = createPSSelectParkingDays(parking.getId());
			    ResultSet rs = st.executeQuery(); 
 			) {
 			while(rs.next()) {      			
 				parking.addOpeningDay(rs.getInt("OPENDAY"));
  	        }
				
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
	}

	private PreparedStatement createPSOpenParkings( int dayOfWeek, int hour ) throws SQLException {
		PreparedStatement st = getConnection().prepareStatement(SQLConstants.SELECT_PARKINGS_FROM_DAY);
		st.setInt( 1, dayOfWeek );
		st.setInt( 2, hour );
		st.setInt( 3, hour );
		st.execute();
		return st;
	}
	public List<Parking> getOpenParkings(boolean onlyAvailables) {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		return getOpenParkings(onlyAvailables, nowAsISO);
	}

	public List<Parking> getOpenParkings(boolean onlyAvailables, String day) {
		//TODO validaciones
		int dayOfWeek = Utils.getWeekDayFromDate(day);
		int hour = Utils.getHourFloor(day);
		List<Parking> openParkings = new ArrayList<Parking>();
		
		try ( PreparedStatement st = createPSOpenParkings(dayOfWeek, hour);
			ResultSet rs = st.executeQuery(); ) {
            while(rs.next()) {
            	Parking p = getParking(rs);
            	addOpeningDays(p);
             	openParkings.add(p);
           }
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return openParkings;
	}
	
	public int takePlace ( int idParking ) {
		//TODO duda validaciones ¿se puede ocupar una plaza en un parking cerrado?
		Parking p = getParking(idParking);
		if ( p == null ) { //TODO pulir
			return ERROR_PARKING_NOT_EXISTS;
		}
		if (p.getFreePlaces() <= 0 ) {
			return ERROR_PARKING_FULL;
		}
		try ( PreparedStatement st = getConnection().prepareStatement(SQLConstants.UPDATE_TAKE_PLACE) ){
			st.setInt(1, idParking);
			st.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return ERROR_UNDEFINED;
		}
		return NO_ERROR;
	}
	public int freePlace ( int idParking ) {
		//TODO duda validaciones ¿se puede liberar una plaza en un parking cerrado?
		Parking p = getParking(idParking);
		if ( p == null ) { //TODO pulir
			return ERROR_PARKING_NOT_EXISTS;
		}
		if ( p.getSize() <= p.getFreePlaces() ) {
			return ERROR_PARKING_EMPTY;
		}
		try ( PreparedStatement st = getConnection().prepareStatement(SQLConstants.UPDATE_FREE_PLACE) ){
			st.setInt(1, idParking);
			st.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return ERROR_UNDEFINED;
		}		
		return NO_ERROR;
	}

	public Connection getConnection() {
		return connection;
	}
	
	
}
